import { useState } from "react";
import { Button } from "react-bootstrap";
import { FaWhatsapp } from "react-icons/fa";

function WhatsAppWidget() {
  const handleWhatsAppClick = () => {
    window.open("https://api.whatsapp.com/send?phone=919696220022&text=Hi", "_blank");
  };

  return (
    <div className="whatsapp-widget">
      <Button
        className="whatsapp-button"
        variant="success"
        onClick={handleWhatsAppClick}
      >
        <FaWhatsapp className="whatsapp-icon" />
      </Button>
    </div>
  );
}

export default WhatsAppWidget;
