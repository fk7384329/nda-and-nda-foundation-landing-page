import React from "react";
import { Col, Container, Image, Row } from "react-bootstrap";

function NdaAndNdsBriefOverview() {
  return (
    <Container>
      <Row>
        <Col xs={12} md={8} className="mt-5">
          <h5>NDA & NDA Foundation (11 & 12)</h5>
          <p>
            Major Kalshi offers a variety of NDA courses to meet the needs of different students. NDA examination is conducted by UPSC twice a year (usually in April & September). Candidates who have passed class 12th or are appearing in class 12th can fill out forms & give the examination
          </p>
          <p>
            For the NDA eligibility, female and male candidates who have completed their 12th or equivalent can apply for the exam. The institute offers both online and offline courses.
          </p>
          <p>
            The online courses are designed for students who want to study from the comfort of their own homes. The offline courses are designed for students who want to study in the classroom setting.
          </p>
          <p>
            The NDA foundation course is a two-year program that prepares students for NDA (National Defense Academy) entrance exam. The course covers all the subjects that are tested in the exam. Major Kalshi Classes is one of the best foundation coaching institutes in India.
          </p>
          <p>
            The institute has a team of experienced and qualified faculty who are experts in preparing for NDA entrance exams. NDA Foundation programs are created specifically for 11th and 12th-grade students. We cover the academic syllabus as well as the NDA syllabus in this program.
          </p>
          <p>
            If a candidate wants to become an officer in the Indian Armed Forces, the NDA Foundation is the best option.
          </p>
        </Col>
        <Col xs={12} md={4}>
          <div className="text-center mt-4 mt-md-5">
            <img src="./images/director.webp" width="80%" alt="" />
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default NdaAndNdsBriefOverview;