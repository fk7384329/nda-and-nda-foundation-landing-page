import React from "react";
import { Card, Col, Container, Row } from "react-bootstrap";

function LatestSelection() {
  return (
    <>
      <section
        className="mt-4"
        style={{
          backgroundColor: "#E6EDF5",
        }}
      >
        <Container className="text-center">
          <h4
            style={{
              paddingTop: "18px",
            }}
          >
            Latest Selection
          </h4>
          <div className="py-4">
            <Row className="justify-content-center">
              <Col>
                <Card className="text-center mt-5">
                  <div
                    style={{
                      position: "relative",
                      top: "-45px",
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className="rounded-circle"
                      style={{
                        width: "150px",
                        height: "150px",
                        border: "2px solid #000",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        marginLeft: "12px",
                      }}
                    >
                      <Card.Img
                        variant="top"
                        src="/images/latest_selection.webp"
                        className="rounded-circle"
                        style={{ width: "120px", height: "120px" }}
                      />
                    </div>
                  </div>
                  <Card.Body>
                    <Card.Title>Devansh Mishra</Card.Title>
                    <Card.Text>
                      <span className="text-muted">#NDA (411)</span>
                      <br />
                      <span>
                        Major Kalshi Classes helped me to reach my true
                        potential and fullfill my dream.
                      </span>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card className="text-center mt-5">
                  <div
                    style={{
                      position: "relative",
                      top: "-45px",
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className="rounded-circle"
                      style={{
                        width: "150px",
                        height: "150px",
                        border: "2px solid #000",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        marginLeft: "12px",
                      }}
                    >
                      <Card.Img
                        variant="top"
                        src="/images/latest_select2.webp"
                        className="rounded-circle"
                        style={{ width: "120px", height: "120px" }}
                      />
                    </div>
                  </div>
                  <Card.Body>
                    <Card.Title>Love Malik</Card.Title>
                    <Card.Text>
                      <span className="text-muted">#TES (AIR 5)</span>
                      <br />
                      <span>
                        Major Kalshi Classes transformed my dreams into reality.
                        Forever grateful!
                      </span>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card className="text-center mt-5">
                  <div
                    style={{
                      position: "relative",
                      top: "-45px",
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className="rounded-circle"
                      style={{
                        width: "150px",
                        height: "150px",
                        border: "2px solid #000",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        marginLeft: "12px",
                      }}
                    >
                      <Card.Img
                        variant="top"
                        src="/images/latest_select3.webp"
                        className="rounded-circle"
                        style={{ width: "120px", height: "120px",backgroundColor:"" }}
                      />
                    </div>
                  </div>
                  <Card.Body>
                    <Card.Title>Yasharth</Card.Title>
                    <Card.Text>
                      <span className="text-muted">#NDA (413)</span>
                      <br />
                      <span>
                        I am grateful to Major Kalshi Classes for providing me
                        such great guidance
                      </span>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </div>
        </Container>
      </section>
    </>
  );
}

export default LatestSelection;
