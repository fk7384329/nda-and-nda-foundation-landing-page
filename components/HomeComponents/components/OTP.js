import React, { useRef, useState } from "react";
import { Form } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

/**
 * Use bootstrap and react-bootstrap, and add thank you model after successful verification.
 */
function OTP({ show, setShow, OTP, handleFormSubmit, setFormSubmitted }) {
  const inputRefs = Array.from({ length: 4 }).map(() => useRef(null));
  const handleClose = () => setShow(false);
  const [verificationStatus, setVerificationStatus] = useState(null);

  const handleResendOTP = () => {
    // Logic for resending the OTP code
  };

  const handleInputChange = (index, e) => {
    const value = e.target.value;

    if (value) {
      if (index < inputRefs.length - 1) {
        inputRefs[index + 1].current.focus();
      } else {
        // Focus on the Verify OTP button if the last input box is filled
        inputRefs[index].current.blur();
      }
    }
  };

  const handleVerifyOTP = () => {
    const otpCode = inputRefs.map((ref) => ref.current.value).join("");
    // Logic for verifying the OTP code

    if (otpCode === OTP) {
      handleFormSubmit();
      setVerificationStatus("success");

      // Store verification status in local storage
      localStorage.setItem("verificationStatus", "success");

      // redirecting to thank you page
      window.open("http://localhost:3000/thankyou", "_blank");

      setTimeout(() => {
        setFormSubmitted(false);
        setShow(false);
      }, 2000);
    } else {
      setVerificationStatus("error");
    }
  };

  return (
    <>
      {verificationStatus !== "success" && (
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Verify OTP</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Please enter the OTP code sent to your phone:</p>
            <Form>
              <div className="d-flex justify-content-center">
                {inputRefs.map((ref, index) => (
                  <Form.Control
                    key={index}
                    ref={ref}
                    type="text"
                    className="otp-input"
                    maxLength={1}
                    autoFocus={index === 0}
                    style={{
                      margin: "0 5px",
                      width: "48px",
                      textAlign: "center",
                    }}
                    onChange={(e) => handleInputChange(index, e)}
                  />
                ))}
              </div>
            </Form>
            {/* Error Message */}
            {verificationStatus === "error" && (
              <p className="text-danger text-center">
                Invalid OTP. Please try again.
              </p>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleVerifyOTP}>
              Verify OTP
            </Button>
            <Button variant="primary" onClick={handleResendOTP}>
              Resend OTP
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
}

export default OTP;
