import React, { useEffect, useState } from "react";
import { Button, Col, Row } from "react-bootstrap";
import { EnquiryForm, OTPbanner } from "..";

// import axios from "axios";
// import { MajorKalshiClassesEnquiryForm } from "@/components/HelperComponents";

function Hero() {
  const [blinking, setBlinking] = useState(false);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setBlinking((prevBlinking) => !prevBlinking);
    }, 500); // Adjust the blinking interval as per your requirement

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  return (
    <div id="mkc_landingpage_hero_section">
      <div
        className="py-2"
        style={{
          backgroundColor: "#E6EDF5",
        }}
      >
        <Row className="">
          {/* Left Content */}
          <Col md={6}>
            <div className="left-content text-center ">
              <div className="hero-content mt-2 mb-4 d-lg-none">
                <Button
                  style={{ backgroundColor: "#003D33", border: 0 }}
                  variant="primary"
                >
                  <span className={blinking ? "blinking" : ""}>
                    Admission Open Now
                  </span>
                </Button>
              </div>
              <img
                src="/images/army_sword.webp"
                alt=""
                className="img-hero img-fluid"
              />
              <div className="hero-content mt-2 mb-4 d-none  d-lg-block">
                <Button
                  style={{ backgroundColor: "#003D33", border: 0 }}
                  variant="primary"
                >
                  <span className={blinking ? "blinking" : ""}>
                    Admission Open Now
                  </span>
                </Button>
              </div>
            </div>
          </Col>

          {/* Form */}
          <Col md={6}>
            <EnquiryForm />
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Hero;
