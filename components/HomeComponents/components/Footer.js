import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { FaFacebookF, FaTwitter, FaInstagram, FaYoutube } from "react-icons/fa";

function Footer() {
  return (
    <footer
      style={{ backgroundColor: "#003D33", color: "white", padding: "40px 0" }}
    >
      <Container>
        <Row>
          <Col md={3}>
            <img
              style={{
                backgroundColor: "white",
                width: "100%",
              }}
              src="./images/mkc_logo.png"
              alt="Logo"
              className="logo"
            />
            {/* <img src="/logo.png" alt="Major Kalshi Classes" style={{ marginBottom: '20px', maxWidth: '100%' }} /> */}
            <p>
              Major Kalshi Classes Pvt. Ltd. is a well known and Trusted Defence
              Organisation in India. Since the last 16 years, this academy
              trained thousand of defence aspirants in India.
            </p>
          </Col>
          <Col md={3}>
            <h5>Quick Links</h5>
            <ul style={{ listStyle: "none", paddingLeft: "0" }}>
              <li>
                <a style={{ color: "white" }} href="/">
                  Home
                </a>
              </li>
              <li>
                <a style={{ color: "white" }} href="/about">About Us</a>
              </li>
              <li>
                <a style={{ color: "white" }} href="/courses">Courses</a>
              </li>
              <li>
                <a style={{ color: "white" }} href="/contact">Contact Us</a>
              </li>
            </ul>
          </Col>
          <Col md={3}>
            <h5>Contact Us</h5>
            <p>
              105/244, Shapath Building, Tagore Town, Prayagraj, Uttar Pradesh -
              211002
            </p>
            <p>Email: support@majorkalshiclasses.com</p>
            <p>Phone: (+91) 9696330033</p>
          </Col>
          <Col md={3}>
            <h5>Follow Us</h5>
            <div>
              <a
                href="https://www.facebook.com/majorkalshiclasses"
                target="_blank"
                rel="noopener noreferrer"
                style={{ color: "white" }}
              >
                <FaFacebookF size={24} style={{ marginRight: "10px" }} />
              </a>
              <a
                href="https://twitter.com/MajKalshiClases"
                target="_blank"
                rel="noopener noreferrer"
                style={{ color: "white" }}
              >
                <FaTwitter size={24} style={{ marginRight: "10px" }} />
              </a>
              <a
                href="https://www.instagram.com/major_kalshi_classes/"
                target="_blank"
                rel="noopener noreferrer"
                style={{ color: "white" }}
              >
                <FaInstagram size={24} style={{ marginRight: "10px" }} />
              </a>
              <a
                href="https://www.youtube.com/c/MajorKalshiclassesPvtLtd"
                target="_blank"
                rel="noopener noreferrer"
                style={{ color: "white" }}
              >
                <FaYoutube size={24} style={{ marginRight: "10px" }} />
              </a>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}

export default Footer;
