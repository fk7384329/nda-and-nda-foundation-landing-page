import React, { useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { OTPbanner } from "..";

function EnquiryForm() {
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [selectedState, setSelectedState] = useState("");
  const [selectedCourse, setSelectedCourse] = useState("");
  const [message, setMessage] = useState("");
  const [formSubmitted, setFormSubmitted] = useState(false);
  const [OTP, setOTP] = useState("");
  const [showOTP, setShowOTP] = useState(false);

  const handleSubmitOTP = async (e) => {
    e.preventDefault();

    console.log("otp form");

    const data = {
      otp: "4517",
    };

    setOTP(data.otp);
    setFormSubmitted(true);
    setShowOTP(true);

    // axios({
    //   method: "post",
    //   url: "https://menu.majorkalshiclasses.com/otp",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   data: {
    //     mobile: phoneNumber,
    //   },
    // })
    //   .then((response) => {
    //     console.log("OTP ResP : ", response);
    //     if (response.status == 200) {
    //       const data = response.data;
    //       setOTP(data.otp);

    //       setFormSubmitted(true);
    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  };

  const handleFormSubmit = async () => {
    const Courses = [
      {
        name: "NDA CLASSROOM",
        code: "168",
        batchId: "270",
      },
      {
        name: "NDA FOUNDATION",
        code: "156",
        batchId: "240",
      },
    ];

    const selectedCourseObj = Courses.find(
      (course) => course.name === selectedCourse
    );

    if (selectedCourseObj) {
      const selectedCourseCode = selectedCourseObj.code;

      const data = {
        sname: name,
        smobile: phoneNumber,
        scourse: selectedCourseCode,
        squery: message,
        enquirypage: "mkcneet",
        sstate: selectedState,
      };

      console.log(data);

      const body = JSON.stringify(data);
      console.log(body);

      // try {
      //   const response = await fetch(
      //     "https://menu.majorkalshiclasses.com/enq/enquiry",
      //     {
      //       method: "POST",
      //       headers: {
      //         "Content-Type": "application/json",
      //       },
      //       body: JSON.stringify(data),
      //     }
      //   );

      //   if (response.ok) {
      //     const data = await response.text();
      //     console.log(data);
      //     setName("");
      //     setPhoneNumber("");
      //     setSelectedCourse("");
      //     setSelectedState("");
      //     setMessage("");

      //     ////
      //   } else {
      //     console.error("Request failed with status:", response.status);
      //   }
      // } catch (error) {
      //   console.error("Error:", error);
      // }
    } else {
      console.log(`Course '${selectedCourse}' not found.`);
    }
  };

  return (
    <>
      {/* OTP verification */}
      {formSubmitted && (
        <OTPbanner
          OTP={OTP}
          handleFormSubmit={handleFormSubmit}
          setFormSubmitted={setFormSubmitted}
          show={showOTP}
          setShow={setShowOTP}
        />
      )}

      <div
        className="right-content enquiry-form mt-4"
        style={{
          backgroundColor: "#fff",
          borderRadius: "10px",
        }}
      >
        <Container className="border p-4">
          <h2
            style={{
              backgroundColor: "#003D33",
              textAlign: "center",
              color: "white",
              padding: "10px",
              fontSize: "18px",
              marginBottom: "20px",
            }}
          >
            Talk to our expert
          </h2>
          <Form id="formSection" onSubmit={handleSubmitOTP}>
            <Form.Group controlId="name" className="mb-3">
              <Form.Control
                required
                aria-describedby="inputGroupPrepend"
                type="text"
                placeholder="Name"
                style={{ fontFamily: "Arial", fontSize: "14px" }}
                value={name}
                onChange={(e) => {
                  e.preventDefault();
                  setName(e.target.value);
                }}
              />
            </Form.Group>

            {/* Contact Field */}
            <Form.Group controlId="contact" className="mb-3">
              <Form.Control
                required
                pattern="[1-9][0-9]{9}"
                type="text"
                placeholder="Contact"
                style={{ fontFamily: "Arial", fontSize: "14px" }}
                value={phoneNumber}
                onChange={(e) => {
                  e.preventDefault();
                  setPhoneNumber(e.target.value);
                }}
              />
            </Form.Group>

            {/* State Field */}
            <Form.Group controlId="state" className="mb-3">
              <Form.Control
                required
                as="select"
                style={{ fontFamily: "Arial", fontSize: "14px" }}
                value={selectedState}
                onChange={(e) => {
                  e.preventDefault();
                  setSelectedState(e.target.value);
                }}
              >
                <option>Select State</option>
                <option value="Andhra Pradesh">Andhra Pradesh</option>
                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                <option value="Assam">Assam</option>
                <option value="Bihar">Bihar</option>
                <option value="Chhattisgarh">Chhattisgarh</option>
                <option value="Goa">Goa</option>
                <option value="Gujarat">Gujarat</option>
                <option value="Haryana">Haryana</option>
                <option value="Himachal Pradesh">Himachal Pradesh</option>
                <option value="Jharkhand">Jharkhand</option>
                <option value="Karnataka">Karnataka</option>
                <option value="Kerala">Kerala</option>
                <option value="Madhya Pradesh">Madhya Pradesh</option>
                <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                <option value="Mizoram">Mizoram</option>
                <option value="Nagaland">Nagaland</option>
                <option value="Odisha">Odisha</option>
                <option value="Punjab">Punjab</option>
                <option value="Rajasthan">Rajasthan</option>
                <option value="Sikkim">Sikkim</option>
                <option value="Tamil Nadu">Tamil Nadu</option>
                <option value="Telangana">Telangana</option>
                <option value="Tripura">Tripura</option>
                <option value="Uttar Pradesh">Uttar Pradesh</option>
                <option value="Uttarakhand">Uttarakhand</option>
                <option value="West Bengal">West Bengal</option>
                <option value="Andaman and Nicobar Islands">
                  Andaman and Nicobar Islands
                </option>
                <option value="Chandigarh">Chandigarh</option>
                <option value="Dadra and Nagar Haveli and Daman and Diu">
                  Dadra and Nagar Haveli and Daman and Diu
                </option>
                <option value="Delhi">Delhi</option>
                <option value="Lakshadweep">Lakshadweep</option>
                <option value="Puducherry">Puducherry</option>
              </Form.Control>
            </Form.Group>

            {/* Course Field */}
            <Form.Group controlId="course" className="mb-3">
              <Form.Control
                as="select"
                style={{ fontFamily: "Arial", fontSize: "14px" }}
                value={selectedCourse}
                onChange={(e) => {
                  e.preventDefault();
                  setSelectedCourse(e.target.value);
                }}
              >
                <option>Select Course</option>
                <option value="NDA CLASSROOM">NDA CLASSROOM</option>
                <option value="NDA FOUNDATION">NDA FOUNDATION</option>
              </Form.Control>
            </Form.Group>

            {/* Message Field */}
            <Form.Group controlId="message" className="mb-3">
              <Form.Control
                as="textarea"
                placeholder="Message"
                rows={3}
                style={{ fontFamily: "Arial", fontSize: "14px" }}
                value={message}
                onChange={(e) => {
                  e.preventDefault();
                  setMessage(e.target.value);
                }}
              />
            </Form.Group>

            {/* Submit Button */}
            <div className="text-center">
              <Button
                style={{
                  backgroundColor: "#003D33",
                  border: 0,
                  textAlign: "center",
                  color: "white",
                  padding: "10px",
                  fontSize: "14px",
                }}
                variant="primary"
                type="submit"
              >
                Send your query
              </Button>
            </div>
          </Form>
        </Container>
      </div>
    </>
  );
}

export default EnquiryForm;
