import React from "react";
import { Carousel, Col, Container, Row } from "react-bootstrap";

function CampusView() {
  return (
    <>
      <div className="mt-4 mb-4">
        <Container>
          <h4 className="text-center mb-4 mt-2">Our Campus View</h4>
        </Container>
        <Container className="d-flex justify-content-center">
          <Row className="d-flex justify-content-center">
            <Carousel
              slide={true}
              interval={null}
              className="d-none d-md-block"
            >
              <Carousel.Item>
                <Row>
                  <Col>
                    <img
                      className="d-block w-100 h-100"
                      src="./images/campus_view1.webp"
                      alt="first slide"
                    />
                  </Col>
                  <Col>
                    <img
                      className="d-block w-100 h-100"
                      src="./images/campus_view7.webp"
                      alt="first slide"
                    />
                  </Col>
                  <Col>
                    <img
                      className="d-block w-100 h-100"
                      src="./images/campus_view2.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
              <Carousel.Item>
                <Row>
                  <Col>
                    <img
                      className="d-block w-100 h-100"
                      src="./images/campus_view4.webp"
                      alt="first slide"
                    />
                  </Col>
                  <Col>
                    <img
                      className="d-block w-100 h-100"
                      src="./images/campus_view5.webp"
                      alt="first slide"
                    />
                  </Col>
                  <Col>
                    <img
                      className="d-block w-100 h-100"
                      src="./images/campus_view6.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
            </Carousel>
            {/* Mobile Version */}
            <Carousel slide={true} interval={null} className="d-md-none">
              <Carousel.Item>
                <Row className="justify-content-center">
                  <Col xs={10} sm={8} md={3} className="text-center">
                    <img
                      className="d-block w-100"
                      src="./images/campus_view1.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
              <Carousel.Item>
                <Row className="justify-content-center">
                  <Col xs={10} sm={8} md={3} className="text-center">
                    <img
                      className="d-block w-100"
                      src="./images/campus_view7.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
              <Carousel.Item>
                <Row className="justify-content-center">
                  <Col xs={10} sm={8} md={3} className="text-center">
                    <img
                      className="d-block w-100"
                      src="./images/campus_view2.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
              <Carousel.Item>
                <Row className="justify-content-center">
                  <Col xs={10} sm={8} md={3} className="text-center">
                    <img
                      className="d-block w-100"
                      src="./images/campus_view4.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
              <Carousel.Item>
                <Row className="justify-content-center">
                  <Col xs={10} sm={8} md={3} className="text-center">
                    <img
                      className="d-block w-100"
                      src="./images/campus_view5.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
              <Carousel.Item>
                <Row className="justify-content-center">
                  <Col xs={10} sm={8} md={3} className="text-center">
                    <img
                      className="d-block w-100"
                      src="./images/campus_view6.webp"
                      alt="first slide"
                    />
                  </Col>
                </Row>
              </Carousel.Item>
            </Carousel>
            <div
              className="mt-4"
              style={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              <button
                style={{
                  borderRadius: "5px",
                  backgroundColor: "#003D33",
                  color:"white",border:"0"
                }}
                className="px-2 py-1"
                onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              >
                Enquiry Now
              </button>
            </div>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default CampusView;
