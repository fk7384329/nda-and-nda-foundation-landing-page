import React from "react";
import { Container, Navbar } from "react-bootstrap";
import { FaPhone, FaCreditCard } from "react-icons/fa";

function Header() {
  return (
    <>
      {/* Header */}
      <Navbar bg="light" expand="lg">
        <Container>
          <div
            className="d-lg-none"
            style={{
              backgroundColor: "#003D33",
              border: "2px solid black",
              width: "100%",
              display: "flex",
              justifyContent: "space-around",
            }}
          >
            <span className="mx-2" type="tel" style={{}}>
              <a
                href="tel:+919696330033"
                target="_blank"
                rel="noopener noreferrer"
                style={{
                  textDecoration: "none",
                  color: "white",
                  border: "0",
                  fontSize: "14px",
                }}
              >
                <FaPhone style={{ marginRight: "5px" }} />
                +919696330033
              </a>
            </span>
            <span className="pay-now-button">
              <a
                href="https://majorkalshiclasses.com/apply-online"
                target="_blank"
                rel="noopener noreferrer"
                style={{
                  textDecoration: "none",
                  color: "white",
                  fontSize: "14px",
                }}
              >
                <FaCreditCard style={{ marginRight: "5px" }} />
                Pay Now
              </a>
            </span>
          </div>
          <Navbar.Brand
            href="/"
            style={
              {
                // border:"2px solid black",
                // margin:"auto"
              }
            }
            className="header_logo"
          >
            <img
              src="./images/mkc_logo.png"
              // style={{ width: "60%", }}
              alt="Logo"
              className="logo header_logo_image"
            />
          </Navbar.Brand>
          <Navbar.Collapse id="basic-navbar-nav">
            <div className="d-flex align-items-center ms-auto">
              <button className="mx-4 d-none d-lg-block" type="tel">
                <a
                  href="tel:+919696330033"
                  target="_blank"
                  rel="noopener noreferrer"
                  style={{
                    textDecoration: "none",
                    color: "black",
                  }}
                >
                  +919696330033
                </a>
              </button>
              <button className="pay-now-button d-none d-lg-block">
                <a
                  href="https://majorkalshiclasses.com/apply-online"
                  target="_blank"
                  rel="noopener noreferrer"
                  style={{
                    textDecoration: "none",
                    color: "black",
                    fontSize: "14px",
                  }}
                >
                  <div style={{
                    display:"flex",
                  }}>
                    <div className="px-2">Pay</div>
                    <div>Now</div>
                  </div>
                </a>
              </button>
            </div>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default Header;
