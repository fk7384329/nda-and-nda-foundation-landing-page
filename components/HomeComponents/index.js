export { default as Header } from "./components/Header";
export { default as Hero } from "./components/Hero";
export { default as NdaAndNdsBriefOverview } from "./components/NdaAndNdsBriefOverview";
export { default as Director } from "./components/Director";
export { default as LatestSelection } from "./components/LatestSelection";
export { default as Footer } from "./components/Footer";
export { default as OTPbanner } from "./components/OTP";
export { default as EnquiryForm } from "./components/Form";
export { default as CampusView } from "./components/CampusView";
