"use client";

import {
  CampusView,
  Footer,
  Header,
  Hero,
  LatestSelection,
  NdaAndNdsBriefOverview,
} from "@/components/HomeComponents";
import "../styles/global.css";
import WhatsAppWidget from "@/components/HelperComponents/components/WhatsAppWidget";

function Home() {
  return (
    <main
      style={{
        width: "100%",
        overflowX: "hidden",
      }}
    >
      {/* Header */}
      <Header />
      {/* Hero Section with lead Form */}
      <Hero />
      {/* NDA/NDS foundation content */}
      <NdaAndNdsBriefOverview />
      {/* Director Image with Message */}
      {/* Latest Selection */}
      <LatestSelection />
      {/* Campus View */}
      <CampusView />
      {/* WhatsApp Widget */}
      <WhatsAppWidget />
      {/* Footer */}
      <Footer />
    </main>
  );
}

export default Home;
