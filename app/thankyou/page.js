"use client";

import { Footer, Header } from "@/components/HomeComponents";
import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import "../../styles/global.css";
import { useRouter } from "next/router";
import Tick from "@/components/HelperComponents/components/Tick";

/**
 * Add a green tick over the h1 thank you message
 */
function page() {
  // const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // Get verificationStatus from local storage
    const verificationStatus = localStorage.getItem("verificationStatus");

    // Simulate an asynchronous delay for demonstration purposes
    setTimeout(() => {
      // If verificationStatus is not "success", redirect to the home page
      if (verificationStatus !== "success") {
        // router.push("/");

        // redirecting to thank you page
        window.location.href = "http://localhost:3000";
      } else if (verificationStatus === "success") {
        setIsLoading(false);
      }
    }, 2000);
  }, []);

  return (
    <>
      <Header />

      <Container className="d-flex align-items-center justify-content-center vh-100">
        <Row>
          <Col>
            {isLoading ? (
              <h1 className="text-center">Loading...</h1>
            ) : (
              <>
                <div className="d-flex justify-content-center">
                  <img src="./images/tick.gif" />
                </div>
                <h1 className="text-center">
                  Thank You!
                  {/* <span style={{ animation: "tickAnimation 1s infinite" }}>
                    &#10004;
                  </span> */}
                </h1>
                <p className="lead text-center">
                  Your Query has been successfully submitted.
                </p>
              </>
            )}
          </Col>
        </Row>
      </Container>

      <Footer />
    </>
  );
}

export default page;
