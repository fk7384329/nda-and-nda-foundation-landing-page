"use client";

import { SSRProvider } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

export default function RootLayout({ children }) {
  return (
    <SSRProvider>
      <html lang="en">
        <body suppressHydrationWarning={true}>{children}</body>
      </html>
    </SSRProvider>
  );
}
